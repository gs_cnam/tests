using System.Runtime.ConstrainedExecution;

namespace creditImmo.Librairy.Tests
{
    
    public class RateCalculatorTests
    {
        [Theory]
        [InlineData(7, "good", 2.56)]
        [InlineData(7, "very good", 2.37)]
        [InlineData(20, "good", 2.90)]
        public void DetermineRate_Returns_RateToUse(int duration, string rateType, double expected)
        {
            double output = RateCalculator.DetermineRate(duration, rateType);
            Assert.Equal(expected, output);
        }

    }
    
}