﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creditImmo.Librairy.Tests
{
    
    public class LoanInssuranceCalculatorTests
    {
        [Theory]
        [InlineData(true, false, false, false, false, 0.25)]
        [InlineData(false, true, false, false, false, 0.45)]
        [InlineData(false, true, true, false, false, 0.75)]
        [InlineData(true, true, true, true, true, 0.80)]
        public void DetermineRate_Returns_RateToUse(bool iSporty, bool isSmoker, bool hasHeartCondition,bool isITEngeneer, bool isFighterPilot, double expected)
        {
            double output = LoanInssuranceCalculator.DetermineLoan(iSporty, isSmoker, hasHeartCondition, isITEngeneer, isFighterPilot);
            Assert.Equal(expected, output);
        }
    }
}
