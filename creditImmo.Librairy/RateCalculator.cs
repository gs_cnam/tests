namespace creditImmo.Librairy
{
    public class RateCalculator
    {
        private static readonly double[,] rates = {
            { 2.56, 2.37, 2.10 }, // rates for duration = 7
            { 2.63, 2.47, 2.15 }, // rates for duration = 10
            { 2.79, 2.67, 2.33 }, // rates for duration = 15
            { 2.90, 2.76, 2.33 }, // rates for duration = 20
            { 3.00, 2.88, 2.45 }  // rates for duration = 25
        };


        public static double DetermineRate(int duration, string rateType)
        {
            int durationIndex = DertermineDurationIndex(duration);
            int rateTypeIndex = DerterminerateTypeIndexIndex(rateType);
            double selectedValue = SelectValueInRatesArray(durationIndex, rateTypeIndex);

            return selectedValue;
        }

        private static double SelectValueInRatesArray(int durationIndex, int rateTypeIndex)
        {
            return rates[durationIndex, rateTypeIndex];
        }

        private static int DertermineDurationIndex(int duration)
        {
            switch (duration)
            {
                case 7:
                    return 0;
                case 10:
                    return 1;
                case 15:
                    return 2;
                case 20:
                    return 3;
                case 25:
                    return 4;
                default:
                    return 0;
            }
        }

        private static int DerterminerateTypeIndexIndex(string rateType)
        {
            switch (rateType.ToLower())
            {
                case "good":
                    return 0;
                case "very good":
                    return 1;
                case "excellent":
                    return 2;
                default: 
                    return 0;
            }
        }

        


    }


}