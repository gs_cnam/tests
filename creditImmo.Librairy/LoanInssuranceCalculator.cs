﻿namespace creditImmo.Librairy
{
    public class LoanInssuranceCalculator
    {
        public static double DetermineLoan(bool isSporty, bool isSmoker, bool hasHeartCondition, bool isITEngeneer, bool isFighterPilot)
        {
            var adjustments = new Dictionary<string, double>()
            {
                { "isSporty", -0.05 },
                { "isSmoker", 0.15 },
                { "hasHeartCondition", 0.30 },
                { "isITEngeneer", -0.05 },
                { "isFighterPilot", 0.15 }
            };

            double loan = 0.30;

            loan += adjustments.Where(x => (x.Key == "isSporty" && isSporty) || (x.Key == "isSmoker" && isSmoker) ||
                                           (x.Key == "hasHeartCondition" && hasHeartCondition) || (x.Key == "isITEngeneer" && isITEngeneer) ||
                                           (x.Key == "isFighterPilot" && isFighterPilot))
                               .Sum(x => x.Value);

            loan = Math.Round(loan, 2);
            return loan;
        }
    }
}
