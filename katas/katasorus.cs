namespace katas.Library
{
    public class katasorus
    {
        public static string DetermineScore(int scorePlayer1, int scorePlayer2)
        {
            string scorePlayer1ToDisplay = FormatScoreToDisplay(scorePlayer1, scorePlayer2);
            string scorePlayer2ToDisplay = FormatScoreToDisplay(scorePlayer2, scorePlayer1);
            return $"{scorePlayer1ToDisplay} - {scorePlayer2ToDisplay}";
        }

        private static string FormatScoreToDisplay(int scorePlayer, int scoreOpponent)
        {
            string result = scorePlayer switch
            {
                0 => "0",
                1 => "15",
                2 => "30",
                3 => "40",
                _ => ""
            };
            if (scorePlayer > 3)
            {
                result = (scorePlayer - scoreOpponent < 2) ? "Advantage" : "Won";
            }
            return result;
        }
    }
}