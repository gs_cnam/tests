namespace katas.Library.Tests
{
    public class katasorusTests
    {
        [Theory]
        [InlineData(0, 0, "0 - 0")]
        [InlineData(1, 0, "15 - 0")]
        [InlineData(0, 1, "0 - 15")]
        [InlineData(1, 1, "15 - 15")]
        [InlineData(2, 0, "30 - 0")]
        [InlineData(3, 0, "40 - 0")]
        [InlineData(4, 3, "Advantage - 40")]
        [InlineData(5, 3, "Won - 40")]
        public void DetermineScore_Returns_ScoreToDisplay(int scorePlayer1, int scorePlayer2, string expected)
        {
            string output = katasorus.DetermineScore(scorePlayer1, scorePlayer2);
            Assert.Equal(expected, output);
        }
    }
}