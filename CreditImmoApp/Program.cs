﻿using creditImmo.Librairy;

internal class Program
{
    private static void Main(string[] args)
    {
        int capital = 175000;
        int duree = 25;
        string taux = "bon";

        bool estSportif = false;
        bool estFumeur = true;
        bool estCardiaque = true;
        bool estInge = true;
        bool estPilote = false;


        double dureeEnMois = duree * 12;
        double tauxMensuel = RateCalculator.DetermineRate(duree, taux);
        double mensualite = capital * tauxMensuel;
        mensualite /= (1 - Math.Pow((1 + tauxMensuel), -1*dureeEnMois));

        double assurance = LoanInssuranceCalculator.DetermineLoan(estSportif, estFumeur, estCardiaque, estInge, estPilote);
        double mensualiteAssurance = assurance * mensualite;
        double totalAssurance = mensualiteAssurance * dureeEnMois;

        Console.WriteLine("tauxMensuel:" + tauxMensuel);
        Console.WriteLine("assurance: " + dureeEnMois);
        Console.WriteLine("mensualite: " + (Math.Pow((1 + tauxMensuel), -1 * dureeEnMois)));

        
    }
}